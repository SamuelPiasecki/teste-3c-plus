import { defineStore } from "pinia";
import axios from "axios";
import { v4 as uuid } from "uuid";
import { db, storage } from "@/firebase-init";
import {
  setDoc,
  getDoc,
  doc,
  getDocs,
  collection,
  updateDoc,
  arrayUnion,
  onSnapshot,
  query,
} from "firebase/firestore";
import { ref, uploadBytes, getDownloadURL } from "firebase/storage";

axios.defaults.baseURL = "http://localhost:4001/";

export const useUserStore = defineStore("user", {
  state: () => ({
    sub: "",
    email: "",
    picture: "",
    name: "",
    chats: [],
    allUsers: [],
    userDataForChat: [],
    showFindClients: false,
    currentChat: null,
    removeUsersFromFindClients: [],
  }),
  actions: {
    async getUserDetailsFromGoogle(data) {
      try {
        let res = await axios.post("api/google-login", {
          token: data.credential,
        });

        let userExists = await this.checkIfUserExists(res.data.sub);
        if (!userExists) await this.saveUserDetails(res);

        this.sub = res.data.sub;
        this.email = res.data.email;
        this.picture = res.data.picture;
        this.name = res.data.name;
      } catch (error) {
        console.log(error);
      }
    },

    async getAllUsers() {
      const querySnapshot = await getDocs(collection(db, "users"));
      let results = [];
      querySnapshot.forEach((doc) => {
        results.push(doc.data());
      });

      if (results.length) {
        this.allUsers = [];
        results.forEach((res) => {
          this.allUsers.push(res);
        });
      }
    },

    async checkIfUserExists(id) {
      const docRef = doc(db, "users", id);
      const docSnap = await getDoc(docRef);
      return docSnap.exists();
    },

    async saveUserDetails(res) {
      try {
        await setDoc(doc(db, "users", res.data.sub), {
          sub: res.data.sub,
          email: res.data.email,
          picture: res.data.picture,
          name: res.data.name,
        });
      } catch (error) {
        console.log(error);
      }
    },

    async getChatById(id) {
      onSnapshot(doc(db, "chat", id), (doc) => {
        let res = [];
        res.push(doc.data());
        this.currentChat = res;
      });
    },

    async getAllChatsByUser() {
      const q = query(collection(db, "chat"));
      onSnapshot(q, (querySnapshot) => {
        let chatArray = [];
        querySnapshot.forEach((doc) => {
          let data = {
            id: doc.id,
            sub1: doc.data().sub1,
            sub2: doc.data().sub2,
            isDone: doc.data().isDone,
            messages: doc.data().messages,
          };

          if (doc.data().sub1 === this.sub) chatArray.push(data);
          if (doc.data().sub2 === this.sub) chatArray.push(data);

          this.removeUsersFromFindClients = [];

          chatArray.forEach((chat) => {
            if (this.sub === chat.sub1) {
              this.allUsers.forEach((user) => {
                if (user.sub == chat.sub2) {
                  chat.user = user;
                  this.removeUsersFromFindClients.push(user.sub);
                }
              });
            }

            if (this.sub === chat.sub2) {
              this.allUsers.forEach((user) => {
                if (user.sub == chat.sub1) {
                  chat.user = user;
                  this.removeUsersFromFindClients.push(user.sub);
                }
              });
            }
          });

          this.chats = [];
          chatArray.forEach((chat) => {
            this.chats.push(chat);
          });
        });
      });
    },

    async finishChat(data) {
      try {
        await updateDoc(doc(db, `chat/${data.chatId}`), {
          isDone: true,
        });
      } catch (error) {
        console.log(error);
      }
    },

    async uploadFiles(file, chatId) {
      try {
        const storageRef = ref(storage, `files/${file.name}`);
        uploadBytes(storageRef, file);
        getDownloadURL(ref(storage, `files/${file.name}`)).then((url) => {
          updateDoc(doc(db, `chat/${chatId}`), {
            messages: arrayUnion({
              sub: this.sub,
              fileName: file.name,
              internal: false,
              file: url,
              createdAt: Date.now(),
            }),
          });
        });
      } catch (error) {
        console.log(error);
      }
    },

    async sendMessage(data) {
      try {
        if (data.chatId) {
          await updateDoc(doc(db, `chat/${data.chatId}`), {
            messages: arrayUnion({
              sub: this.sub,
              message: data.message,
              internal: data.internal,
              createdAt: Date.now(),
            }),
          });
        } else {
          let id = uuid();
          await setDoc(doc(db, `chat/${id}`), {
            sub1: this.sub,
            sub2: data.sub2,
            picture: this.picture,
            isDone: false,
            name: this.name,
            createdAt: Date.now(),
            messages: [
              {
                internalMessage: false,
                sub: this.sub,
                message: data.message,
                createdAt: Date.now(),
              },
            ],
          });

          this.userDataForChat[0].id = id;
          this.showFindClients = false;
        }
      } catch (error) {
        console.log(error);
      }
    },

    logout() {
      this.sub = "";
      this.email = "";
      this.picture = "";
      this.name = "";
      this.allUsers = [];
      this.userDataForChat = [];
      this.showFindClients = false;
      this.chats = [];
      this.currentChat = null;
      this.removeUsersFromFindClients = [];
    },
  },
  persist: true,
});
