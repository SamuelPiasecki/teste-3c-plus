import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCoojJHqZVt39edzb9R3m_rJqS2f4wDW8g",
  authDomain: "teste-3c-plus.firebaseapp.com",
  projectId: "teste-3c-plus",
  storageBucket: "teste-3c-plus.appspot.com",
  messagingSenderId: "829808602795",
  appId: "1:829808602795:web:21e4b58efe4bb7715a91ee",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const storage = getStorage(app);

export { db, storage };
